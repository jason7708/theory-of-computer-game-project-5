#pragma once
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include <vector>
#include "board.h"
#include "action.h"
#include "weight.h"
#include <fstream>
#include <float.h>

class weight_agent;
class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args) {
		if (meta.find("init") != meta.end()) // pass init=... to initialize the weight
			init_weights(meta["init"]);
		if (meta.find("load") != meta.end()) // pass load=... to load from a specific file
			load_weights(meta["load"]);
	}
	virtual ~weight_agent() {
		if (meta.find("save") != meta.end()) // pass save=... to save to a specific file
			save_weights(meta["save"]);
	}
	weight& operator[] (size_t i) { return net[i]; }
	const weight& operator[] (size_t i) const { return net[i]; }

protected:
	virtual void init_weights(const std::string& info) {
		//net.emplace_back(65536); // create an empty weight table with size 65536
		//net.emplace_back(65536); // create an empty weight table with size 65536
		// now net.size() == 2; net[0].size() == 65536; net[1].size() == 65536
		//RRRRRRRRRRRRRRRRRRRRRR
		/*
		net.emplace_back(50625); //15^4
		net.emplace_back(50625); //15^4
		*/

		net.emplace_back(11390625*4);  //15^6 * 11 (hint)
		net.emplace_back(11390625*4);  //15^6 * 11 (hint)
		net.emplace_back(11390625*4);  //15^6 * 11 (hint)
		net.emplace_back(11390625*4);  //15^6 * 11 (hint)

		for(unsigned i=0; i<net[0].size(); i++){
			net[0][i] = 0;
			net[1][i] = 0;
			net[2][i] = 0;		//6
			net[3][i] = 0;		//6
		}
		//RRRRRRRRRRRRRRRRRRRRRR
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) std::exit(-1);
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
};

/**
 * base agent for agents with a learning rate
 */
class learning_agent : public agent {
public:
	learning_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~learning_agent() {}
	float get_alpha(){
		return alpha;
	}

protected:
	float alpha;
};
/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
/*
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }), popup(0, 9) {}

	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		for (int pos : space) {
			if (after(pos) != 0) continue;
			board::cell tile = popup(engine) ? 1 : 2;
			return action::place(pos, tile);
		}
		return action();
	}

private:
	std::array<int, 16> space;
	std::uniform_int_distribution<int> popup;
};
*/
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
class rndenv : public weight_agent {
public:
	int slide_direction;
	rndenv(const std::string& args = "") : weight_agent("name=random role=environment " + args), bonus(0), total_tile(0) {
		tile_bag.clear();
		slide_direction=5;
		times = 0;
		bonus = 0;
		total_tile = 0;
		hint_tile = 3;
	}
	void get_user_direction(const int user_move) {
		slide_direction = user_move;
	}
	void clear_bag(void) {
		tile_bag.clear();
	}
	virtual void open_episode(const std::string& flag = "") {
		tile_bag.clear();
		slide_direction=5;
		times = 0;
		bonus = 0;
		total_tile = 0;
		hint_tile = 3;
	}
	void pop_from_bag(int tile){
		std::vector<int>::iterator begin = tile_bag.begin();
		for(unsigned i = 0;i < tile_bag.size();i++){
			if(tile_bag[i] == tile){
				tile_bag.erase(begin+i);
				return;
			}
		}
	}
	virtual action take_action(board& after){
		//std::cout << "debug777\n";
		//board::cell tile = 0;

		if(tile_bag.empty()){
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
		}

		//get direction
		if(times < 9){ //first nine
			slide_direction = 5;
			times++;
		}
		else{
			slide_direction = after.move_direction;
			//std::cout << slide_direction << "//////////////////////\n";
		}
		// get posible position
		if(slide_direction == 5) {
			initial_space={0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
			int init_nine[9] = {3, 1, 1, 3, 2, 2, 2, 2, 3};
			int init_nine_pos[9] = {0, 1, 2, 3, 4, 7, 8, 11, 15};
			int tile = hint_tile;
			if(times != 9){
				hint_tile = init_nine[times];
				after.info(init_nine[times]);
			}
			else{
				hint_tile = 3;
				after.info(3);
			}
			total_tile++;
			int pos = init_nine_pos[times-1];
			pop_from_bag(tile);
			return action::place(pos, tile);
		}
		else if(slide_direction == 0){
			space={12, 13, 14, 15};   //up
		}
		else if(slide_direction == 1){
			space={0, 4, 8, 12};   //right
		}
		else if(slide_direction == 2){
			space={0, 1, 2, 3};   //down
		}
		else if(slide_direction == 3){
			space={3, 7, 11, 15};   //left
		}
		if(hint_tile == 1 || hint_tile == 2 || hint_tile == 3){
				pop_from_bag(hint_tile);
		}
		if(tile_bag.empty()){
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (1);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (2);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
			tile_bag.push_back (3);
		}
		int next_hint = -1;
		int final_pos = -1;
		float tmp_min = FLT_MAX;
		bool hint_avil[4] = {false, false, false, false};
		//bonus or not
		if(((float)(bonus+1)/total_tile <= 1.0/21 )&& after.BonusOn()){
			//std::uniform_int_distribution<int> bonus_num(4, after.MaxTile()-3);
			//tile = bonus_num(engine);
			hint_avil[3] = true;
		}
		//check 1, 2, 3 is in the bag or not
		for(unsigned i = 0 ; i < tile_bag.size() ; i++){
			hint_avil[tile_bag[i] - 1 ] = true;
		}
		//std::cout << "silde:" << slide_direction << "\n";
		if(hint_tile != 4){
			for(int pos : space){
				if (after(pos) != 0) continue;
				float max[4] = {FLT_MIN, FLT_MIN, FLT_MIN, FLT_MIN};
				int go[4] = {-1, -1, -1, -1};
				for(int i = 1 ; i <= 4 ; i++){
					if(hint_avil[i-1] == false){
						go[i-1] = -2;
						continue;
					}
					for(int op = 0; op <= 3 ; op++){ //suppose player move
						board a = after;
						a.place(pos, hint_tile);
						int reward = a.slide(op);
						if(reward == -1){
							continue;
						}
						float value_next = get_board_value(a, i);
					
						if((value_next+reward) >= max[i-1]){
							go[i-1] = op;
							max[i-1] = value_next+reward;
						}
					}
				}
				float minimum = FLT_MAX;
				for(int i = 0; i <= 3; i++){ //choose the hint_next which is lowest
					if(go[i] == -2) continue; //i is not in the bag
					if(go[i] == -1){ // player lose
						final_pos = pos;
						next_hint = i+1;
					}
					if(max[i] <= minimum){
						minimum = max[i];
						next_hint = i+1;
					}
					//std::cout << "db1 878787878787\n";
				}
				if(minimum <= tmp_min){
					tmp_min = minimum;
					final_pos = pos;
				}
				//std::cout << "db2 878787878787\n";
			}
		}
		else{  //bonus
			for(int pos : space){
				if (after(pos) != 0) continue;
				float max[4] = {FLT_MIN, FLT_MIN, FLT_MIN, FLT_MIN};
				int go[4] = {-1, -1, -1, -1};
				for(int i = 1 ; i <= 4 ; i++){
					if(hint_avil[i-1] == false){
						go[i-1] = -2;
						continue;
					}
					for(int op = 0; op <= 3 ; op++){ //suppose player move
						board a = after;
						float value_bonus = FLT_MAX;
						int bonus_choose = -1;
						for(int j = 4 ; j <= (after.MaxTile()-3); j++){
							board b = a;
							b.place(pos, j);
							float value_tmp = get_board_value(b, i);
							if(value_tmp < value_bonus){
								value_bonus = value_tmp;
								bonus_choose = j;
							}
						}
						hint_tile = bonus_choose;
						a.place(pos, hint_tile);
						int reward = a.slide(op);
						if(reward == -1){
							continue;
						}
						float value_next = get_board_value(a, i);
					
						if((value_next+reward) >= max[i-1]){
							go[i-1] = op;
							max[i-1] = value_next+reward;
						}
					}
				}
				float minimum = FLT_MAX;
				for(int i = 0; i <= 3; i++){ //choose the hint_next which is lowest
					if(go[i] == -2) continue; //i is not in the bag
					if(go[i] == -1){ // player lose
						final_pos = pos;
						next_hint = i+1;
					}
					if(max[i] <= minimum){
						minimum = max[i];
						next_hint = i+1;
					}
					
				}
				if(minimum <= tmp_min){
					tmp_min = minimum;
					final_pos = pos;
				}
				
			}
		}
		//std::cout << next_hint << "////////////////////////////////////\n";
		//std::cout << "next_hint:" << next_hint << "\n";
		//std::cout << "final_pos:" << final_pos << "\n";
		//std::cout << "hint_tile:" << hint_tile << "\n";
		//std::cout << "bag:";
		/*
		for(int i = 0 ; i < tile_bag.size() ; i++){
			std::cout << tile_bag[i] << ",";
		}
		*/
		if(next_hint == -1 || final_pos == -1){
			std::cout << "error debug777\n";
			return action();
		}
		else{
			int tile = hint_tile;
			hint_tile = next_hint;
			after.info(next_hint);
			if(next_hint == 4){
				bonus++;
			}
			total_tile++;
			return action::place(final_pos, tile);
		}
		return action();
	}
	float get_board_value(board a, int hint_now){
		float value = 0;
		value += net[0][(a(0)+a(1)*15+a(2)*15*15+a(3)*15*15*15+a(4)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(3)+a(7)*15+a(11)*15*15+a(15)*15*15*15+a(2)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(15)+a(14)*15+a(13)*15*15+a(12)*15*15*15+a(11)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(12)+a(8)*15+a(4)*15*15+a(0)*15*15*15+a(13)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(3)+a(2)*15+a(1)*15*15+a(0)*15*15*15+a(7)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(0)+a(4)*15+a(8)*15*15+a(12)*15*15*15+a(1)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(12)+a(13)*15+a(14)*15*15+a(15)*15*15*15+a(8)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[0][(a(15)+a(11)*15+a(7)*15*15+a(3)*15*15*15+a(14)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(4)+a(5)*15+a(6)*15*15+a(7)*15*15*15+a(8)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(2)+a(6)*15+a(10)*15*15+a(14)*15*15*15+a(1)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(11)+a(10)*15+a(9)*15*15+a(8)*15*15*15+a(7)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(13)+a(9)*15+a(5)*15*15+a(1)*15*15*15+a(14)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(7)+a(6)*15+a(5)*15*15+a(4)*15*15*15+a(11)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(1)+a(5)*15+a(9)*15*15+a(13)*15*15*15+a(2)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(8)+a(9)*15+a(10)*15*15+a(11)*15*15*15+a(4)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[1][(a(14)+a(10)*15+a(6)*15*15+a(2)*15*15*15+a(13)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(0)+a(1)*15+a(2)*15*15+a(4)*15*15*15+a(5)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(3)+a(7)*15+a(11)*15*15+a(2)*15*15*15+a(6)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(15)+a(14)*15+a(13)*15*15+a(11)*15*15*15+a(10)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(12)+a(8)*15+a(4)*15*15+a(13)*15*15*15+a(9)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(3)+a(2)*15+a(1)*15*15+a(7)*15*15*15+a(6)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(0)+a(4)*15+a(8)*15*15+a(1)*15*15*15+a(5)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(12)+a(13)*15+a(14)*15*15+a(8)*15*15*15+a(9)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[2][(a(15)+a(11)*15+a(7)*15*15+a(14)*15*15*15+a(10)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(4)+a(5)*15+a(6)*15*15+a(8)*15*15*15+a(9)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(2)+a(6)*15+a(10)*15*15+a(1)*15*15*15+a(5)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(11)+a(10)*15+a(9)*15*15+a(7)*15*15*15+a(6)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(13)+a(9)*15+a(5)*15*15+a(14)*15*15*15+a(10)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(7)+a(6)*15+a(5)*15*15+a(11)*15*15*15+a(10)*15*15*15*15+a(9)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(1)+a(5)*15+a(9)*15*15+a(2)*15*15*15+a(6)*15*15*15*15+a(10)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(8)+a(9)*15+a(10)*15*15+a(4)*15*15*15+a(5)*15*15*15*15+a(6)*15*15*15*15*15)*4+(hint_now-1)];
		value += net[3][(a(14)+a(10)*15+a(6)*15*15+a(13)*15*15*15+a(9)*15*15*15*15+a(5)*15*15*15*15*15)*4+(hint_now-1)];
		return value;
	}

private:
	std::vector<int> tile_bag;
	std::array<int, 4> space;
	std::array<int, 16> initial_space;
	//std::uniform_int_distribution<int> bonus_p;
	int bonus;
	int total_tile;
	int times;
	int hint_tile;
};
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
/**
 * dummy player
 * select a legal action randomly
 */
class player : public weight_agent {
public:
	player(const std::string& args = "") : weight_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }),/*RRRR*/ player_direction(0)/*RRRR*/ {}

	int player_choose(void){
		return player_direction;
	}	
	virtual action take_action(board& before) {
		
		int go = -1;
		float max = FLT_MIN;
		int lose = -1;
		board a;

		for (int op : opcode) {
			a = before;
			board::reward reward = a.slide(op);
			if(reward == -1){
				continue;
			}
			if(reward != -1){
				lose = 0;
			}
			float value_next = 0;
			/* 4 tuple
			value_next += net[0][a(0)+a(1)*15+a(2)*15*15+a(3)*15*15*15]; //a's value
			value_next += net[1][a(4)+a(5)*15+a(6)*15*15+a(7)*15*15*15];
			value_next += net[1][a(8)+a(9)*15+a(10)*15*15+a(11)*15*15*15];
			value_next += net[0][a(12)+a(13)*15+a(14)*15*15+a(15)*15*15*15];

			value_next += net[0][a(0)+a(4)*15+a(8)*15*15+a(12)*15*15*15];
			value_next += net[1][a(1)+a(5)*15+a(9)*15*15+a(13)*15*15*15];
			value_next += net[1][a(2)+a(6)*15+a(10)*15*15+a(14)*15*15*15];
			value_next += net[0][a(3)+a(7)*15+a(11)*15*15+a(15)*15*15*15];
			*/
			//6 tuple
			value_next += net[0][(a(0)+a(1)*15+a(2)*15*15+a(3)*15*15*15+a(4)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(3)+a(7)*15+a(11)*15*15+a(15)*15*15*15+a(2)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(15)+a(14)*15+a(13)*15*15+a(12)*15*15*15+a(11)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(12)+a(8)*15+a(4)*15*15+a(0)*15*15*15+a(13)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(3)+a(2)*15+a(1)*15*15+a(0)*15*15*15+a(7)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(0)+a(4)*15+a(8)*15*15+a(12)*15*15*15+a(1)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(12)+a(13)*15+a(14)*15*15+a(15)*15*15*15+a(8)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[0][(a(15)+a(11)*15+a(7)*15*15+a(3)*15*15*15+a(14)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(4)+a(5)*15+a(6)*15*15+a(7)*15*15*15+a(8)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(2)+a(6)*15+a(10)*15*15+a(14)*15*15*15+a(1)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(11)+a(10)*15+a(9)*15*15+a(8)*15*15*15+a(7)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(13)+a(9)*15+a(5)*15*15+a(1)*15*15*15+a(14)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(7)+a(6)*15+a(5)*15*15+a(4)*15*15*15+a(11)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(1)+a(5)*15+a(9)*15*15+a(13)*15*15*15+a(2)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(8)+a(9)*15+a(10)*15*15+a(11)*15*15*15+a(4)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[1][(a(14)+a(10)*15+a(6)*15*15+a(2)*15*15*15+a(13)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(0)+a(1)*15+a(2)*15*15+a(4)*15*15*15+a(5)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(3)+a(7)*15+a(11)*15*15+a(2)*15*15*15+a(6)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(15)+a(14)*15+a(13)*15*15+a(11)*15*15*15+a(10)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(12)+a(8)*15+a(4)*15*15+a(13)*15*15*15+a(9)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(3)+a(2)*15+a(1)*15*15+a(7)*15*15*15+a(6)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(0)+a(4)*15+a(8)*15*15+a(1)*15*15*15+a(5)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(12)+a(13)*15+a(14)*15*15+a(8)*15*15*15+a(9)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[2][(a(15)+a(11)*15+a(7)*15*15+a(14)*15*15*15+a(10)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(4)+a(5)*15+a(6)*15*15+a(8)*15*15*15+a(9)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(2)+a(6)*15+a(10)*15*15+a(1)*15*15*15+a(5)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(11)+a(10)*15+a(9)*15*15+a(7)*15*15*15+a(6)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(13)+a(9)*15+a(5)*15*15+a(14)*15*15*15+a(10)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(7)+a(6)*15+a(5)*15*15+a(11)*15*15*15+a(10)*15*15*15*15+a(9)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(1)+a(5)*15+a(9)*15*15+a(2)*15*15*15+a(6)*15*15*15*15+a(10)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(8)+a(9)*15+a(10)*15*15+a(4)*15*15*15+a(5)*15*15*15*15+a(6)*15*15*15*15*15)*4+(a.info()-1)];
			value_next += net[3][(a(14)+a(10)*15+a(6)*15*15+a(13)*15*15*15+a(9)*15*15*15*15+a(5)*15*15*15*15*15)*4+(a.info()-1)];
			if((reward+value_next)>=max){
				go = op;
				max = reward+value_next;
			}

		}
		if (lose != -1){
				//RR
				player_direction = go;

				//RR
				return action::slide(go);
		}
		else{

			return action();
		}
	}
private:
	std::array<int, 4> opcode;
	int player_direction; //RRRRRRRRRRRRRR
};
